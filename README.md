# Weibo

微博


知識點 Git
一開始先到gitlab創建一個 project，包含ReadME
然後 Clone with SSH ，接著到終端機找到自己要放的資料夾 cd ...
再 git clone 貼上剛剛的地址，接著到資料夾會出現你在 gitlab 創好的 project 名稱的資料夾
ex. CameraFilter，點擊該資料夾會發現裡面有 .git 和一個 ReadMe 文件，這時候可以開啟 sourceTree，把 CameraFilter 資料夾拉到目錄裡，然後就可以開啟新專案了，新專案要跟 .git 和 ReadMe 同一個目錄下


知識點
在tabbar中間設置一個按鈕

知識點
響應者鏈條 @objc
當看到下面這種function，其中的含意是
1. 使用者觸發event (ex.點擊按鈕)，導致消息循環runloop醒來
2. runloop去尋找對象，準備發送消息
3. 應用程序監聽到事件之後，先由UIApplication從上至下傳遞，找到誰最適合響應這次事件的控件
4. 找到後，再由下至上傳遞找到UIApplication要監聽的對象，把消息傳給他，如對象響應這個消息，就會執行消息(像下面的clickComposedButton)
@objc private func clickComposedButton() {
    print("good")
}

知識點
當想要純代碼和SB都加載某個視圖，可以這樣寫
//使用 SB & XIB 開發加載的函數
required init?(coder: NSCoder) {

    //移除 fatalError
    //fatalError("init(coder:) has not been implemented")
    //加上以下 code
    super.init(coder: coder)
    let btn = UIButton(type: .contactAdd)
    addSubview(btn)
    btn.addTarget(self, action: #selector(click), for: .touchUpInside)
}

//使用純代碼開發使用的
override init(frame: CGRect) {

    super.init(frame: frame)
    let btn = UIButton(type: .contactAdd)
    addSubview(btn)
    btn.addTarget(self, action: #selector(click), for: .touchUpInside)
}

知識點
/*
    VFL : 可視化格式語言
    H 水平方向
    V 垂直方向
    | 邊界
    [] 包裝控件
    views: 是一個字典 [名字：控件名] - VFL 字串中表示控件的字串
    metrics: 是一個字典 [名字：NSNumber] - VFL 字串中表示某一個數值
 */
NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[background]-0-|", options: [], metrics: nil, views: ["background":backgroundView]))

NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[background]-(btnHeight)-[regButton]", options: [], metrics: ["btnHeight":-36], views: ["background":backgroundView, "regButton":registerButton]))

知識點
///開啟首頁轉輪動畫
private func startAnim() {
    let anim = CABasicAnimation(keyPath: "transform.rotation")
    anim.toValue = 2 * Double.pi
    anim.repeatCount = MAXFLOAT
    anim.duration = 20
    //用在不斷重複的動畫上，當動畫綁定的圖層對應的視圖被銷毀，動畫會自動被銷毀
    anim.isRemovedOnCompletion = false
    iconView.layer.add(anim, forKey: nil)
}

知識點
統計代碼行數
到終端機
1. cd 文件拉進來
2. find . -name "*.swift" | xargs wc -l


//創建應用程序建立的
//不可以修改
client_id
App Key: 206221103
//用來識別應用程序的，可以更改
App Secret: 8a3fe743e908cc3cb43d8022baac07ca

//自己設置的，用來識別應用程序
redirect_url
ReDirectUrl: http://www.baidu.com

1. 請求驗證
https://api.weibo.com/oauth2/authorize?client_id=206221103&redirect_uri=http://www.baidu.com

如果授權成功
http://www.baidu.com/?code=144cf8f2dea129af4908eaafdf8b6551

code叫做授權碼 -> 令牌

https://api.weibo.com/2/statuses/public_timeline.json?access_token=2.00DTQ9GI0pWRxNb828a9cd891zaHBC

我們獲得的令牌
2.00DTQ9GI0pWRxN6e161b0e38XyXdhD


知識點 
計算型屬性可以寫在 extension
-see語法可以直接導入網址
///OAuth 授權 URL
/// - see: [https://open.weibo.com/wiki/Oauth2/authorize](https://open.weibo.com/wiki/Oauth2/authorize)
var oauthURL: URL {
    let urlString = "https://api.weibo.com/oauth2/authorize?client_id=\(appKey)&redirect_uri=\(redirectUrl)"
    return URL(string: urlString)!
}


知識點
///自動填充用戶名和密碼 - web 注入 (以代碼的方式向 web 頁面添加內容)
@objc private func autoFill() {
    let js = "document.getElementById('userId').value = '00886900625360';" + "document.getElementById('passwd').value = 'qxzQ*:yicC:fV9K';"
    
    //讓 webView 執行 js
    webView.stringByEvaluatingJavaScript(from: js)
}


知識點
///判斷帳戶是否過期
private var isExpired: Bool {
    
    //判斷用戶帳戶過期日期與當前系統日期進行比較
    //如果 A compare B 配 orderedDescending ，說明 A較大，也就是沒過期
    //如果 A compare B 配 orderedAscending ，說明 A較小，也就是過期
    if account?.expireDate?.compare(Date()) == ComparisonResult.orderedDescending {
        return false
    }
    //如果過期返回 true
    return true
}


知識點
//設置界面，視圖的層次結構
loadView
//視圖加載完後的後續處理，通常用來設置數據
viewDidLoad
//視圖已經顯示，通常可以顯示動畫/鍵盤鍵盤處理
viewDidAppear
 
 
知識點
{ get } 是只讀的意思


知識點
使用自動約束，不能在設置 frame
- 自動佈局系統會根據設置的約束，自動計算控件的 frame
- 自動佈局系統會在 layoutSubviews 函數中設置 frame
- 如果程序員主動修改 frame，會引起自動佈局系統計算錯誤
- 工作原理：當有一個運行循環啟動，自動佈局系統會收集所有的約束變化
- 在運行循環結束前，調用 layoutSubviews 函數統一設置 frame
- 如果希望某些約束提前更新使用，使用 layoutIfNeeded 函數讓自動佈局系統提前更新當前收集到的約束變化


知識點
當解析 json 遇到字典嵌套時，在模型的構造函數裡要多寫一個 setValuesForKeys(dict)
然後加上下面這段 (舉例)，針對字典裡的字典再處理

override func setValue(_ value: Any?, forKey key: String) {
    if key == "user" {
        if let dict = value as? [String: Any] {
            user = User(dict: dict)
        }
    }
}


知識點
-使用 UIImage imageNamed 創建的圖像，緩存由系統管理，程序員不能直接釋放
-適用的圖像，小的圖片素材
系統會保存在內存中，並不會創建很多次，不會因為圖片創建，造成延遲，也不會造成系統內存大量消耗
-注意：高清大圖，千萬不要用這個方法，要用 contentOfFile 方式加載


知識點
如果在 TableView裡面， UILabel 出現大量數字，這時如何避免超出畫面，除了將 numberOfLine 設為0，還有一招
contentLabel.preferredMaxLayoutWidth = UIScreen.main.bounds.width - 2 * StatusCellMargin


知識點
計算 tableView 行高
1. tableView.estimatedRowHeight = 200
2. tableView.rowHeight = UITableView.automaticDimension
3. 找到從上往下底部的控件，指定向下的約束
//指定向下的約束 (以下是用 snapKit 套件示範)
make.bottom.equalTo(contentView.snp_bottom).offset(-StatusCellMargin)


知識點
網路應用程序開發順序
1. 打通數據通道 (印出數據驗證)
2. 字典轉模型為 UI 準備數據源
3. UI 佈局
4. 綁定數據
5. 監聽響應事件


知識點 
closure
提前準備好的代碼( ex. self.tableView.reloadData( ) )，在需要的時候執行 ( ex. 完成回調
finished(true) )

//呼叫 load 函數，帶入一個閉包參數
load (
    { (text) in
        print("hello", text)   //提前準備好的代碼
    }
)

//被呼叫後，往下執行到 finished 的時候，又要執行沒有名字的函數(匿名函數)，於是回到上面的閉包執行，並把text傳過去
func load(finished: (text: String) -> Void ) {
    finished(text: "王老五")   //完成回調
}


知識點
視圖模型更新後，到 cell 要按照大小更新 pictureView 的高和寬
///微博視圖模型
var viewModel: StatusViewModel? {
didSet {
//自動計算大小
//會呼叫 sizeThatFits，然後改變 pictureView.bounds 的寬高
sizeToFit()
}
}

override func sizeThatFits(_ size: CGSize) -> CGSize {
return calcViewSize()
}

//測試修改配圖視圖高度 - 實際開發中要注意，如果動態修改約束的高度
//可能導致行高計算錯誤
//設置配圖視圖 - 設置之後，配圖視圖有能力計算大小
//模型設值後， 原本的 pictureView 大小就改掉了(因為呼叫 sizeThatFits )
pictureView.viewModel = viewModel
//改掉之後，如果是 storyboard 會變黃色警告，必須按照大小更新 pictureView 的高和寬
pictureView.snp_updateConstraints { (make) in
    make.height.equalTo(pictureView.bounds.height)
    make.width.equalTo(pictureView.bounds.width)
}


計算視圖大小
private func calcViewSize() -> CGSize {
    
    //1. 準備
    //每行照片數量
    let rowCount: CGFloat = 3
    //最大寬度 (一行三張照片)
    let maxWidth = UIScreen.main.bounds.width - 2 * StatusCellMargin
    let itemWidth = (maxWidth - 2 * StatusPictureViewItemMargin) / rowCount
    
    //2. 設置 layout 的 itemSize
    let layout = collectionViewLayout as! UICollectionViewFlowLayout
    layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
    
    //3. 獲取圖片數量
    let count = viewModel?.thumbnailUrls?.count ?? 0
    
    //計算開始
    //1> 沒有圖片
    if count == 0 {
        return .zero
    }
    //2> 一張圖片
    if count == 1 {
        //TODO: - 臨時指定大小
        let size = CGSize(width: 150, height: 120)
        //內部圖片的大小
        layout.itemSize = size
        //配圖視圖的大小
        return size
    }
    //3> 四張圖片 2 x 2 的大小
    if count == 4 {
        let width = 2 * itemWidth + StatusPictureViewItemMargin
        return CGSize(width: width, height: width)
    }
    //4> 其他圖片，按照九宮格來顯示
    //計算出行數
    let row = CGFloat((count - 1) / Int(rowCount) + 1)
    let height = row * itemWidth + (row - 1) * StatusPictureViewItemMargin
    let width = rowCount * itemWidth + (rowCount - 1) * StatusPictureViewItemMargin
    return CGSize(width: width, height: height)
}


知識點
collectionView 自己也可以當自己的 dataSource
要註冊 reuseID 和 dataSource = self
寫在構造函數裡
//MARK: - 構造函數
init() {
    let layout = UICollectionViewFlowLayout()
    super.init(frame: .zero, collectionViewLayout: layout)
  
    //設置數據源 - 自己當自己的數據源
    //應用場景：自定義視圖的小框架
    dataSource = self
    
    //註冊可重用 cell
    register(UICollectionViewCell.self, forCellWithReuseIdentifier: StatusPictureCellId)
}
//刷新數據 - 如果不刷新，後續的 collectionView 一旦被復用，不再調用數據源方法
viewModel 裡面記得要 reloadData()，不然 cell 會變很奇怪，不會顯示正確

刷新 row 高度流程
先呼叫 heightForRowAt > rowHeight > 啟動 viewModel didSet > 啟動 pictureView.viewModel didSet > sizeToFit > sizeThatFits > calcViewSize > sizeThatFits > reloadData()

行高
   -- 設置了預估行高 estimatedRowHeight
   * 當前顯示的行高法會調用三次 (每個版本的 Xcode 調用次數可能不同)

   問題：預估行高如果不同，計算的次數不同
   1. 使用預估行高，計算出預估的 contentSize
   2. 根據預估行高，判斷計算次數，順序計算每一行的行高，更新 contentSize
   3. 如果預估行高過大，超出預估範圍，順序計算後續行高，一直到填滿屏幕退出，同時更新 contentSize
   4. 使用預估行高，每個 cell 的顯示前需要計算，單個 cell 的效率是低的，從整體效率高！

   執行順序：行數 -> 每個 cell 都會調用 -> 行高
   預估行高：盡量靠近！

   -- 沒設置預估行高 estimatedRowHeight
   * 1. 計算所有行的高度
   * 2. 再計算顯示行的高度

   執行順序：行數 -> 行高 -> cell

   為什麼要調用所有行高方法？
   UITableView 繼承自 UIScrollView
   表格視圖滾動非常流暢 -> 需要提前計算出 contentSize


知識點
不讓點擊後 cell 變灰
selectionStyle = .none
//MARK: - 構造函數
override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupUI()
    selectionStyle = .none
}


知識點
新增功能需求，為了盡量不要動到原本的 class，可以考慮用繼承的方式 
ex. 加入轉發微博 cell


知識點
緩存單張圖片
如果是一張圖片的狀況下，要在 reloadData 的時候取得他的 size 
必須在下載完網路數據後，調用 cacheSingleImage 進行單張圖片緩存


知識點
上/下拉更新
由後端定義的參數去判斷
///since_id: 若指定此参数，则返回ID比since_id大的微博（即比since_id时间晚的微博），默认为0。
///max_id: 若指定此参数，则返回ID小于或等于max_id的微博，默认为0。
首先在 NetworkTools 的 loadStatus 添加這兩個參數，並且將此兩參數加到 params

//判斷是否下拉
if since_id > 0 {
    params["since_id"] = since_id
} else if max_id > 0 {  //上拉參數
    params["max_id"] = max_id - 1
}

接著去 StatusListViewModel 改，loadStatus 新增一個參數 isPullup，用來判斷上拉還是下拉

//下拉刷新 - 數組中第一條微博的 id
let since_id = isPullup ? 0 : (statusList.first?.status.id ?? 0)
//上拉刷新 - 數組中最後一條微博的 id
let max_id = isPullup ? (statusList.last?.status.id ?? 0) : 0

loadStatus 的內部也要判斷如何拼接數據

//3. 拼接數據
//判斷是否是上拉刷新
if max_id > 0 {
    self.statusList += dataList
} else {
    self.statusList = dataList + self.statusList
}

在 controller 部分，用 isPullup: pullupView.isAnimating 判斷上拉還是下拉即可


知識點
設置 UICollectionViewFlowLayout 可以在原本的 class 直接創一個 class
意思是，內部的 class 只能被原有的 class 訪問，並且可以直接繼承 prepare 方法去修改 collectionLayout
//MARK: - 表情佈局（類中類 - 只允許被包含的類使用）
class EmoticonLayout: UICollectionViewFlowLayout {
    
    // collectionView 第一次佈局的時候被調用
    // collectionView 的尺寸已經設置好了
    override func prepare() {
        super.prepare()
        
        let col: CGFloat = 7
        let row: CGFloat = 3
        let w = collectionView!.bounds.width / col
        let margin = (collectionView!.bounds.height - row * w) * 0.5
        itemSize = CGSize(width: w, height: w)
        minimumLineSpacing = 0
        minimumInteritemSpacing = 0
        sectionInset = UIEdgeInsets(top: margin, left: 0, bottom: margin, right: 0)
    }
}


知識點
-黃色文件夾：編譯後，文件在 mainBundle 中，源代碼程序需要通過這種方式拖曳添加
注意不能出現重名的文件
效率較高

-藍色文件夾：編譯後，文件在 mainBundle 中，遊戲文件的素材通過這種方式添加
允許出現重名文件
用於換膚應用
效率略差

-白色 Bundle （將資料夾後面加上.bundle即可）：編譯後，文件在 mainBundle 中仍然以包的形式存在，可以路徑形式訪問
允許重名文件
拖曳文件更簡單
主要用於第三方框架包裝資源素材


知識點
如果圖像名稱使用 "" 會報錯誤 Invalid asset name supplied:


知識點
創造一個 toolbar，內部假設要放五個 item，如果這五個 item 要觸發的事件都不一樣
可以這樣寫

1. 先定義一個字典 (假設此 item 需要有圖像和點擊事件)
let itemSettings = [
    ["imageName":"imagebackground"],
    ["imageName":"at"],
    ["imageName":"number"],
    ["imageName":"emoji", "actionName":"selectEmoticon"],
    ["imageName":"more"]
]

2. 跑迴圈建立 item
這裡在創建 UIBarButtonItem 的時候，要注意不是用之前創建的方式，而是用 UIBarButtonItem(customView: button) 創建，然後利用字典鍵值取得事件的 function name，再用 Selector(actionName) 帶入 addTarget

var items = [UIBarButtonItem]()
for dict in itemSettings {
    let button = UIButton(imageName: dict["imageName"]!)
    
    //判斷 actionName
    if let actionName = dict["actionName"] {
        button.addTarget(self, action: Selector(actionName), for: .touchUpInside)
    }
    
    let item = UIBarButtonItem(customView: button)
    items.append(item)
    items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil))
}


知識點
更新 layout 佈局
照片控制器視圖由於是後加的，所以要將本來的 view 做一些調整
textView 原先是底部連 toolBar 的上方，要用 snp_remakeConstraints 重設 textView layout，並將底部設為 picturePickerController.view 的上方


知識點
iOS 6.0 的 view 的 x.y 是以狀態欄下方開始計算
如果有 navBar，iOS 6.0 x.y 是從導航欄下方開始計算
iOS 6.0 同樣不包含 tabBar
如果是滾動視圖( tableView ) - 無法穿透
穿透效果是靠 scrollView - contentInset 把內容滾到 navBar 下方
( 原點從 statusBar bounds 開始算)


知識點
如果遇到嵌套很多的 view 或 controller，要傳直到最外面會有點麻煩
這時候可以用 notification，直接從最裡面 post，最外面去 addObserver


知識點
如果要在 imageView 上面畫圖，必須先創一個 UIView，override draw 方法，然後把 UIView 加到 imageView 裡面
一旦給構造函數指定了參數，系統不再提供默認的構造函數，所以要用 init()


知識點
自定義轉場的三步驟
let vc = PhotoBrowserViewController(urls: urls, indexPath: indexPath)
//設置 modal 的類型是自定義類型
vc.modalPresentationStyle = .custom
//設置動畫代理
vc.transitioningDelegate = self?.photoBrowserAnimator
self?.present(vc, animated: true, completion: nil)

然後要自定義一個 photoBrowserAnimator 的懶加載子控件，型別是 PhotoBrowserAnimator

class PhotoBrowserAnimator: NSObject, UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
}

UIViewControllerTransitioningDelegate 裡有兩個方法
//1. 誰來提供這個動畫
func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return self
}
//2. 誰來提供解除這個轉場動畫
func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {

}

UIViewControllerAnimatedTransitioning 裡主要實現兩個方法
//1. 動畫時長
func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return 0.5
}
//2. 實現具體的動畫效果 - 一旦實現了此方法，所有的動畫都交由程序員負責
//transitionContext: 轉場動畫的上下文 - 提供動畫所需要的素材
func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
    
}
