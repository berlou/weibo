//
//  MainViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/28.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {
    
    //MARK: - 監聽方法
    @objc private func clickComposedButton() {
        
        //判斷用戶是否登入
        var vc: UIViewController
        if UserAccountViewModel.sharedUserAccount.userLogon {
            vc = ComposeViewController()
        } else {
            vc = OAuthViewController()
        }
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        present(nav, animated: true, completion: nil)
    }
    
    //MARK: - 視圖生命週期函數
    override func viewDidLoad() {
        super.viewDidLoad()

        addChildViewControllers()
        setupComposedButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //把按鈕移到最前面
        tabBar.bringSubviewToFront(composedButton)
    }
    
    //MARK: - 懶加載控件
    private lazy var composedButton: UIButton = UIButton(imageName: "plus")
}

//MARK: 設置界面
extension MainViewController {
    
    private func setupComposedButton() {
        tabBar.addSubview(composedButton)
        
        //設定按鈕位置
        let count = children.count
        //讓按鈕寬一點，可解決手指不小心觸摸到空白處
        let width = tabBar.bounds.width / CGFloat(count) - 1
        composedButton.frame = tabBar.bounds.insetBy(dx: 2 * width, dy: 0)
        composedButton.addTarget(self, action: #selector(clickComposedButton), for: .touchUpInside)
    }
    
    /// 添加所有的控制器
    private func addChildViewControllers() {
        
        addChildViewController(vc: HomeTableViewController(), title: "首頁", imageName: "home-7")
        addChildViewController(vc: MessageTableViewController(), title: "消息", imageName: "message-7")
        addChild(UIViewController())
        addChildViewController(vc: DiscoverTableViewController(), title: "發現", imageName: "search-7")
        addChildViewController(vc: ProfileTableViewController(), title: "我", imageName: "user-7")
    }
    
    /// 添加控制器
    private func addChildViewController(vc:UIViewController, title:String, imageName:String?) {
        vc.title = title
        vc.tabBarItem.image = UIImage(named: imageName ?? "")
        let nav = UINavigationController(rootViewController: vc)
        addChild(nav)
    }
}
