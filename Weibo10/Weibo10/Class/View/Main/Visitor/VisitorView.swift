//
//  VisitorView.swift
//  Weibo10
//
//  Created by Derek on 2020/3/29.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import SnapKit

///訪客視圖 - 處理用戶未登錄的介面顯示
class VisitorView: UIView {
    
    //MARK: - 設置視圖信息
    func setupInfo(imageName: String?, title: String) {
        messageLabel.text = title
        //如果圖片名稱為 nil, 說明是首頁，直接返回
        guard let imageName = imageName else {
            startAnim()
            return
        }
        iconView.isHidden = true
        homeView.image = UIImage(named: imageName)
    }
    
    ///開啟首頁轉輪動畫
    private func startAnim() {
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        anim.toValue = 2 * Double.pi
        anim.repeatCount = MAXFLOAT
        anim.duration = 20
        //用在不斷重複的動畫上，當動畫綁定的圖層對應的視圖被銷毀，動畫會自動被銷毀
        anim.isRemovedOnCompletion = false
        iconView.layer.add(anim, forKey: nil)
    }
    
    //MARK: - 構造函數
    //使用純代碼開發使用的
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    //使用 SB & XIB 開發加載的函數
    required init?(coder: NSCoder) {
        //阻止使用 SB 使用當前自定義視圖
        //如果只希望當前視圖被純代碼的方式加載，就使用fatalError
        //fatalError("init(coder:) has not been implemented")
        super.init(coder: coder)
        setupUI()
    }
    
    //MARK: - 懶加載控件
    private lazy var iconView: UIImageView = UIImageView(image: #imageLiteral(resourceName: "circle"))
    private lazy var backgroundView: UIImageView = UIImageView(image: #imageLiteral(resourceName: "background"), contentMode: .scaleAspectFill)
    private lazy var homeView: UIImageView = UIImageView(image: #imageLiteral(resourceName: "home-run"))
    private lazy var messageLabel: UILabel = UILabel(title: "關注一些人，回這裡看看有什麼驚喜")
    lazy var registerButton: UIButton = UIButton(title: "註冊", color: .red)
    lazy var loginButton: UIButton = UIButton(title: "登錄", color: .white)
}

extension VisitorView {
    ///設置界面
    private func setupUI() {
        addSubview(backgroundView)
        addSubview(iconView)
        addSubview(homeView)
        addSubview(messageLabel)
        addSubview(registerButton)
        addSubview(loginButton)
        
        iconView.snp_makeConstraints { (make) in
            make.centerX.equalTo(self.snp_centerX)
            make.centerY.equalTo(self.snp_centerY).offset(-60)
            make.width.equalTo(160)
            make.height.equalTo(200)
        }
        homeView.snp_makeConstraints { (make) in
            make.centerX.equalTo(iconView.snp_centerX)
            make.centerY.equalTo(iconView.snp_centerY).offset(-5)
            make.width.equalTo(100)
            make.height.equalTo(150)
        }
        messageLabel.snp_makeConstraints { (make) in
            make.centerX.equalTo(iconView.snp_centerX)
            make.top.equalTo(iconView.snp_bottom).offset(16)
            make.width.equalTo(250)
            make.height.equalTo(36)
        }
        registerButton.snp_makeConstraints { (make) in
            make.left.equalTo(messageLabel.snp_left).offset(10)
            make.top.equalTo(messageLabel.snp_bottom).offset(16)
            make.width.equalTo(100)
            make.height.equalTo(36)
        }
        loginButton.snp_makeConstraints { (make) in
            make.right.equalTo(messageLabel.snp_right).offset(-10)
            make.top.equalTo(messageLabel.snp_bottom).offset(16)
            make.width.equalTo(100)
            make.height.equalTo(36)
        }
        backgroundView.snp_makeConstraints { (make) in
            make.top.equalTo(self.snp_top)
            make.left.equalTo(self.snp_left)
            make.right.equalTo(self.snp_right)
            make.bottom.equalTo(self.snp_bottom)
        }
        
        /*
        for v in subviews {
            v.translatesAutoresizingMaskIntoConstraints = false
        }
        */
        
        /*
        //圖標
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1.0, constant: -60))
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 160))
        addConstraint(NSLayoutConstraint(item: iconView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200))
        addConstraint(NSLayoutConstraint(item: homeView, attribute: .centerX, relatedBy: .equal, toItem: iconView, attribute: .centerX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: homeView, attribute: .centerY, relatedBy: .equal, toItem: iconView, attribute: .centerY, multiplier: 1.0, constant: -5))
        addConstraint(NSLayoutConstraint(item: homeView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        addConstraint(NSLayoutConstraint(item: homeView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 150))
        
        //文字
        addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .centerX, relatedBy: .equal, toItem: homeView, attribute: .centerX, multiplier: 1.0, constant: 0))
        addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .top, relatedBy: .equal, toItem: homeView, attribute: .bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 250))
        addConstraint(NSLayoutConstraint(item: messageLabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 36))
        
        //註冊按鈕
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: .left, relatedBy: .equal, toItem: messageLabel, attribute: .left, multiplier: 1.0, constant: 10))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: .top, relatedBy: .equal, toItem: messageLabel, attribute: .bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        addConstraint(NSLayoutConstraint(item: registerButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 36))
        
        //登入按鈕
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: .right, relatedBy: .equal, toItem: messageLabel, attribute: .right, multiplier: 1.0, constant: -10))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: .top, relatedBy: .equal, toItem: messageLabel, attribute: .bottom, multiplier: 1.0, constant: 16))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 100))
        addConstraint(NSLayoutConstraint(item: loginButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 36))
        
        /*
            VFL : 可視化格式語言
            H 水平方向
            V 垂直方向
            | 邊界
            [] 包裝控件
            views: 是一個字典 [名字：控件名] - VFL 字串中表示控件的字串
            metrics: 是一個字典 [名字：NSNumber] - VFL 字串中表示某一個數值
         */
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[background]-0-|", options: [], metrics: nil, views: ["background":backgroundView]))
        NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[background]-0-|", options: [], metrics: nil, views: ["background":backgroundView]))
       
        /*  <範例> NSLayoutConstraint.activate(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[background]-(btnHeight)-[regButton]", options: [], metrics: ["btnHeight":-36], views: ["background":backgroundView, "regButton":registerButton]))
         */
        */
    }
}
