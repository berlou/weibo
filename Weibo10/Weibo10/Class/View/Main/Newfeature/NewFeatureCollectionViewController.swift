//
//  NewFeatureCollectionViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/31.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import SnapKit

//可重用 CellId
private let WENewFeatureViewCellId = "WENewFeatureViewCellId"
private let WENewFeatureImageCount = 3

class NewFeatureCollectionViewController: UICollectionViewController {

    //懶加載屬性，必須在控制器實例化後才會被創建
    //private lazy var layout = UICollectionViewFlowLayout()
    
    //MARK: - 構造函數
    init() {
        // super 指定的構造函數
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = UIScreen.main.bounds.size
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.scrollDirection = .horizontal
        
        //構造函數，完成之後內部屬性才會被創建
        super.init(collectionViewLayout: layout)
        collectionView.isPagingEnabled = true
        collectionView.bounces = false
        collectionView.showsHorizontalScrollIndicator = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.backgroundColor = .white
        self.collectionView.register(NewFeatureCell.self, forCellWithReuseIdentifier: WENewFeatureViewCellId)
    }

    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return WENewFeatureImageCount
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: WENewFeatureViewCellId, for: indexPath) as! NewFeatureCell
        cell.imageIndex = indexPath.item
        return cell
    }
    
    // ScrollView 停止滾動方法
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //在最後一頁才調用動畫方法
        //根據 contentOffset 計算頁數
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        
        //判斷是否是最後一頁
        if page != WENewFeatureImageCount - 1 {
            return
        }
        
        // Cell 播放動畫
        let cell = collectionView.cellForItem(at: IndexPath(item: page, section: 0)) as! NewFeatureCell
        cell.showButtonAnim()
    }
}

//MARK: - 新特性 Cell
private class NewFeatureCell: UICollectionViewCell {
    
    var imageIndex = 0 {
        didSet {
            iconView.image = UIImage(named: "bg\(imageIndex + 1)")
            startButton.isHidden = true
        }
    }
    
    @objc func clickStartButton() {
        print("開始體驗")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: WBSwitchRootViewControllerNotification), object: nil)
    }
    
    ///顯示按鈕動畫
    func showButtonAnim() {
        startButton.isHidden = false
        startButton.isUserInteractionEnabled = false
        startButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        UIView.animate(withDuration: 1.6,            //動畫時長
                       delay: 0,                   //延時時間
                       usingSpringWithDamping: 0.6,//彈力係數 0~1，越小越彈
                       initialSpringVelocity: 10,  //初始速度，模擬重力加速度
                       options: [],                //動畫選項
                       animations: {
                        self.startButton.transform = .identity
        }) { (_) in
            print("OK")
            self.startButton.isUserInteractionEnabled = true
        }
    }
    
    // frame 的大小是 layout.itemSize 指定的
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        addSubview(iconView)
        addSubview(startButton)
        iconView.frame = bounds
        
        startButton.snp_makeConstraints { (make) in
            make.centerX.equalTo(self.snp_centerX)
            make.bottom.equalTo(self.snp_bottom).multipliedBy(0.95)
            make.width.equalTo(100)
        }
        
        startButton.addTarget(self, action: #selector(clickStartButton), for: .touchUpInside)
    }
    
    //MARK: - 懶加載控件
    private lazy var iconView = UIImageView()
    private lazy var startButton = UIButton(title: "開始體驗", color: .white, bgColor: .orange, fontSize: 16)
}
