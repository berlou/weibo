//
//  StatusCellBottomView.swift
//  Weibo10
//
//  Created by Derek on 2020/4/2.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///底部視圖
class StatusCellBottomView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - 懶加載控件
    ///轉發按鈕
    private lazy var retweetedButton = UIButton(title: " 轉發", fontSize: 12, color: .darkGray, image: #imageLiteral(resourceName: "share"))
    ///評論按鈕
    private lazy var commentButton = UIButton(title: " 評論", fontSize: 12, color: .darkGray, image: #imageLiteral(resourceName: "comment"))
    ///點讚按鈕
    private lazy var likeButton = UIButton(title: " 贊", fontSize: 12, color: .darkGray, image: #imageLiteral(resourceName: "like"))
}

//MARK: - 設置介面
extension StatusCellBottomView {
    private func setupUI() {
        backgroundColor = UIColor(white: 0.9, alpha: 1.0)
        
        addSubview(retweetedButton)
        addSubview(commentButton)
        addSubview(likeButton)
        
        retweetedButton.snp_makeConstraints { (make) in
            make.top.equalTo(snp_top)
            make.left.equalTo(snp_left)
            make.bottom.equalTo(snp_bottom)
        }
        commentButton.snp_makeConstraints { (make) in
            make.top.equalTo(retweetedButton.snp_top)
            make.left.equalTo(retweetedButton.snp_right)
            make.width.equalTo(retweetedButton.snp_width)
            make.height.equalTo(retweetedButton.snp_height)
        }
        likeButton.snp_makeConstraints { (make) in
            make.top.equalTo(commentButton.snp_top)
            make.left.equalTo(commentButton.snp_right)
            make.width.equalTo(commentButton.snp_width)
            make.height.equalTo(commentButton.snp_height)
            make.right.equalTo(snp_right)
        }
        
        //佈局
        let width = 0.5
        let scale = 0.4
        let sep1 = setupView()
        let sep2 = setupView()
        addSubview(sep1)
        addSubview(sep2)
        sep1.snp_makeConstraints { (make) in
            make.left.equalTo(retweetedButton.snp_right)
            make.centerY.equalTo(retweetedButton.snp_centerY)
            make.width.equalTo(width)
            make.height.equalTo(retweetedButton.snp_height).multipliedBy(scale)
        }
        sep2.snp_makeConstraints { (make) in
            make.left.equalTo(commentButton.snp_right)
            make.centerY.equalTo(commentButton.snp_centerY)
            make.width.equalTo(width)
            make.height.equalTo(commentButton.snp_height).multipliedBy(scale)
        }
    }
    
    ///分隔視圖
    private func setupView() -> UIView {
        let view = UIView()
        view.backgroundColor = .darkGray
        return view
    }
}
