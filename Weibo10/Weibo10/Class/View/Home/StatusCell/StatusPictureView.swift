//
//  StatusPictureView.swift
//  Weibo10
//
//  Created by Derek on 2020/4/2.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit
import SDWebImage

///照片之間的間距
private let StatusPictureViewItemMargin: CGFloat = 8
///可重用標示符
private let StatusPictureCellId = "StatusPictureCellId"

class StatusPictureView: UICollectionView {

    ///微博視圖模型
    var viewModel: StatusViewModel? {
        didSet {
            //自動計算大小
            //會呼叫 sizeThatFits，然後改變 pictureView.bounds 的寬高
            sizeToFit()
            
            //刷新數據 - 如果不刷新，後續的 collectionView 一旦被復用，不再調用數據源方法
            reloadData()
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return calcViewSize()
    }
    
    //MARK: - 構造函數
    init() {
        let layout = UICollectionViewFlowLayout()
        //設置間距
        layout.minimumLineSpacing = StatusPictureViewItemMargin
        layout.minimumInteritemSpacing = StatusPictureViewItemMargin
        
        super.init(frame: .zero, collectionViewLayout: layout)
        backgroundColor = UIColor(white: 0.8, alpha: 1.0)
        
        //設置數據源 - 自己當自己的數據源
        //應用場景：自定義視圖的小框架
        dataSource = self
        delegate = self
        
        //註冊可重用 cell
        register(StatusPictureCell.self, forCellWithReuseIdentifier: StatusPictureCellId)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: - UICollectionViewDataSource
extension StatusPictureView: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("點擊照片\(indexPath) \(viewModel?.thumbnailUrls)")
        
        //測試動畫轉場協議函數
        //photoBrowserPresentFromRect(indexPath: indexPath)
        //photoBrowserPresentToRect(indexPath: indexPath)
        
        //明確問題：傳遞什麼數據？當前 URL 的數組 / 當前用戶選中的索引
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: WBStatusSelectedPhotoNotification), object: self, userInfo: [WBStatusSelectedPhotoIndexPathKey:indexPath, WBStatusSelectedPhotoURLsKey:viewModel?.thumbnailUrls
        ])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.thumbnailUrls?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StatusPictureCellId, for: indexPath) as! StatusPictureCell
        cell.imageURL = viewModel?.thumbnailUrls?[indexPath.item]
        return cell
    }
}

//MARK: - 照片查看器的展現協議
extension StatusPictureView: PhotoBrowserPresentDelegate {
    
    ///創建一個 imageView 來參與動畫
    func imageViewForPresent(indexPath: IndexPath) -> UIImageView {
        let iv = UIImageView()
        
        //1. 設置內容填充模式
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        //2. 設置圖像 (縮圖的緩存) - SDWebImage 如果已經存在本地緩存，不會發起網路請求
        if let url = viewModel?.thumbnailUrls?[indexPath.item] {
           iv.sd_setImage(with: url)
        }
        
        return iv
    }
    
    ///動畫起始位置
    func photoBrowserPresentFromRect(indexPath: IndexPath) -> CGRect {
        //1. 根據 indexPath 獲得當前用戶選擇的 cell
        let cell = self.cellForItem(at: indexPath)!
        //2. 通過 cell 知道 cell 對應在屏幕上的準確位置
        //在不同視圖之間的座標系的轉換，self. 是 cell 的父視圖
        //由 collectionView 將 cell 的 frame 位置轉換到 keyWindow 對應的 frame 位置
        let rect = self.convert(cell.frame, to: UIApplication.shared.keyWindow)
//        let v = imageViewForPresent(indexPath: indexPath)
//        v.frame = rect
//        UIApplication.shared.keyWindow?.addSubview(v)
        
        return rect
    }
    
    ///目標位置
    func photoBrowserPresentToRect(indexPath: IndexPath) -> CGRect {
        
        //根據縮圖的大小等比例計算目標位置
        guard let key = viewModel?.thumbnailUrls?[indexPath.item].absoluteString else {
            return .zero
        }
        //從 SDWebImage 獲取本地緩存圖片
        guard let image = SDImageCache.shared.imageFromDiskCache(forKey: key) else {
            return .zero
        }
        //根據圖像大小，計算全屏的大小
        let w = UIScreen.main.bounds.width
        let h = image.size.height * w / image.size.width
        
        //對高度進行額外處理
        let screenHeight = UIScreen.main.bounds.height
        var y:CGFloat = 0
        if h < screenHeight { //圖片短，垂直居中顯示
            y = (screenHeight - h) * 0.5
        }
        
        let rect = CGRect(x: 0, y: y, width: w, height: h)
        //let v = imageViewForPresent(indexPath: indexPath)
        //v.frame = rect
        //UIApplication.shared.keyWindow?.addSubview(v)
        
        return rect
    }
}


//MARK: - 計算視圖大小
extension StatusPictureView {
    
    private func calcViewSize() -> CGSize {
        
        //1. 準備
        //每行照片數量
        let rowCount: CGFloat = 3
        //最大寬度 (一行三張照片)
        let maxWidth = UIScreen.main.bounds.width - 2 * StatusCellMargin
        let itemWidth = (maxWidth - 2 * StatusPictureViewItemMargin) / rowCount
        
        //2. 設置 layout 的 itemSize
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        
        //3. 獲取圖片數量
        let count = viewModel?.thumbnailUrls?.count ?? 0
        
        //計算開始
        //1> 沒有圖片
        if count == 0 {
            return .zero
        }
        //2> 一張圖片
        if count == 1 {
            var size = CGSize(width: 150, height: 120)
            //利用 SDWebImage 檢查本地的緩存圖像 - key 就是 url 完整字符串
            let key = viewModel?.thumbnailUrls?.first?.absoluteString
            SDWebImageManager.shared.imageCache.queryImage(forKey: key, options: [], context: nil) { (image, _, _) in
                if let image = image {
                    size = image.size
                }
            }
            
            //過窄處理 - 針對長圖
            size.width = size.width < 40 ? 40 : size.width
            //過寬的圖片
            if size.width > 300 {
                let w: CGFloat = 300
                let h = size.height * w / size.width
                size = CGSize(width: w, height: h)
            }
            
            //內部圖片的大小
            layout.itemSize = size
            //配圖視圖的大小
            return size
        }
        //3> 四張圖片 2 x 2 的大小
        if count == 4 {
            let width = 2 * itemWidth + StatusPictureViewItemMargin
            return CGSize(width: width, height: width)
        }
        //4> 其他圖片，按照九宮格來顯示
        //計算出行數
        let row = CGFloat((count - 1) / Int(rowCount) + 1)
        let height = row * itemWidth + (row - 1) * StatusPictureViewItemMargin
        let width = rowCount * itemWidth + (rowCount - 1) * StatusPictureViewItemMargin
        return CGSize(width: width, height: height)
    }
}

//MARK: - 微博配圖 cell
private class StatusPictureCell: UICollectionViewCell {
    
    var imageURL: URL? {
        didSet {
            iconView.sd_setImage(with: imageURL, placeholderImage: nil, options: [.retryFailed,    // SD 超時時長 15s，一旦超時會記入黑名單
                 .refreshCached]) // 如果 URL 不變，圖像變
        }
    }
    
    //MARK: - 構造函數
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        contentView.addSubview(iconView)
        //設置佈局 - 因為 cell 會變化，另外不同的 cell 大小可能不一樣
        iconView.snp_makeConstraints { (make) in
            make.edges.equalTo(contentView.snp_edges)
        }
    }
    
    //MARK: - 懶加載控件
    private lazy var iconView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
}
