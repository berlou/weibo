//
//  StatusCell.swift
//  Weibo10
//
//  Created by Derek on 2020/4/2.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

///微博 Cell 中控件的間距數值
let StatusCellMargin: CGFloat = 12
///微博頭像的高度
let StatusCellIconWidth: CGFloat = 35
///微博認證圖標的高度
let StatusCellVipIconWidth: CGFloat = 17

///微博 Cell
class StatusCell: UITableViewCell {
    
    ///微博視圖模型
    var viewModel: StatusViewModel? {
        didSet {
            //設置工作
            topView.viewModel = viewModel
            contentLabel.text = viewModel?.status.text
            
            //測試修改配圖視圖高度 - 實際開發中要注意，如果動態修改約束的高度
            //可能導致行高計算錯誤
            //設置配圖視圖 - 設置之後，配圖視圖有能力計算大小
            //模型設值後， 原本的 pictureView 大小就改掉了(因為呼叫 sizeThatFits )
            pictureView.viewModel = viewModel
            //改掉之後，如果是 storyboard 會變黃色警告，必須按照大小更新 pictureView 的高和寬
            pictureView.snp_updateConstraints { (make) in
                make.height.equalTo(pictureView.bounds.height)
                make.width.equalTo(pictureView.bounds.width)
                
                //根據配圖數量，決定配圖視圖的頂部間距
                //let offset = viewModel?.thumbnailUrls?.count ?? 0 > 0 ? StatusCellMargin : 0
                //make.top.equalTo(contentLabel.snp_bottom).offset(offset)
            }
        }
    }
    
    ///根據指定的視圖模型計算行高
    func rowHeight(vm: StatusViewModel) -> CGFloat {
        
        //1. 記錄視圖模型 -> 會調用上面的 didSet 設置內容以及更新約束
        viewModel = vm
        //2. 強制更新所有約束 -> 所有控件的 frame 都會被計算正確
        contentView.layoutIfNeeded()
        //3. 返回底部視圖的最大高度
        return bottomView.frame.maxY
    }
    
    //MARK: - 構造函數
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
        selectionStyle = .none
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - 懶加載控件
    ///頂部視圖
    private lazy var topView = StatusCellTopView()
    ///微博正文標籤
    lazy var contentLabel = UILabel(title: "微博正文", fontSize: 15, screenInset: StatusCellMargin)
    ///配圖視圖
    lazy var pictureView = StatusPictureView()
    ///底部視圖
    lazy var bottomView = StatusCellBottomView()
}

//MARK: - 設置介面
extension StatusCell {
    
    @objc func setupUI() {
        contentView.addSubview(topView)
        contentView.addSubview(contentLabel)
        contentView.addSubview(pictureView)
        contentView.addSubview(bottomView)
        
        topView.snp_makeConstraints { (make) in
            make.top.equalTo(contentView.snp_top)
            make.left.equalTo(contentView.snp_left)
            make.right.equalTo(contentView.snp_right)
            make.height.equalTo(2 * StatusCellMargin + StatusCellIconWidth)
        }
        contentLabel.snp_makeConstraints { (make) in
            make.top.equalTo(topView.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(contentView.snp_left).offset(StatusCellMargin)
        }
        
        bottomView.snp_makeConstraints { (make) in
            make.top.equalTo(pictureView.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(contentView.snp_left)
            make.right.equalTo(contentView.snp_right)
            make.height.equalTo(44)
            
            //指定向下的約束
            //make.bottom.equalTo(contentView.snp_bottom)
        }
    }
}
