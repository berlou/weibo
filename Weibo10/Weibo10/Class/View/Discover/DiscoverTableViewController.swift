//
//  DiscoverTableViewController.swift
//  Weibo10
//
//  Created by Derek on 2020/3/28.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

class DiscoverTableViewController: VisitorTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        visitorView?.setupInfo(imageName: "chat", title: "登錄後，最新、最熱微博盡在掌握")
    }
}
