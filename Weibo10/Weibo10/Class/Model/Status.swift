//
//  Status.swift
//  Weibo10
//
//  Created by Derek on 2020/4/1.
//  Copyright © 2020 Derek. All rights reserved.
//

import Foundation

class Status: NSObject {
    ///微博ID
    var id: Int = 0
    ///微博信息内容
    var text: String?
    ///微博创建时间
    var created_at: String?
    ///微博来源
    var source: String?
    ///用戶模型
    var user: User?
    ///縮圖配圖數組 key: thumbnail_pic
    var pic_urls: [[String: String]]?
    ///被转发的原微博信息字段
    var retweeted_status: Status?
    
    //如果使用 KVC 時，value 是一個字典，會直接給屬性轉換成字典
    init(dict: [String: Any]) {
        super.init()
        self.id = dict["id"] as? Int ?? 0
        self.text = dict["text"] as? String
        self.created_at = dict["created_at"] as? String
        self.source = dict["source"] as? String
        self.pic_urls = dict["pic_urls"] as? [[String: String]]
        setValuesForKeys(dict)
    }
    
    override func setValue(_ value: Any?, forKey key: String) {
        if key == "user" {
            if let dict = value as? [String: Any] {
                user = User(dict: dict)
            }
        } else if key == "retweeted_status" {
            if let dict = value as? [String: Any] {
                retweeted_status = Status(dict: dict)
            }
        }
    }
}
