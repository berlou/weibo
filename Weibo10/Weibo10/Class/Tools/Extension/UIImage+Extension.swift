//
//  UIImage+Extension.swift
//  PhootSelection
//
//  Created by Derek on 2020/4/12.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

extension UIImage {
    
    //將圖像縮放到指定寬度
    //如果給定的圖片寬度小於指定寬度，直接返回
    func scaleToWith(width: CGFloat) -> UIImage {
        
        //1. 判斷寬度
        if width > size.width {
            return self
        }
        
        //2. 計算比例
        let height = size.height * width / size.width
        let rect = CGRect(x: 0, y: 0, width: width, height: height)
        
        //3. 使用核心繪圖繪製新的圖像
        //1> 開啟上下文
        UIGraphicsBeginImageContext(rect.size)
        //2> 繪圖 - 在指定區域拉伸繪製
        self.draw(in: rect)
        //3> 取結果
        let result = UIGraphicsGetImageFromCurrentImageContext()
        //4> 關閉上下文 - 釋放內存
        UIGraphicsEndImageContext()
        //5> 返回結果
        return result!
    }
}
