//
//  UIButton+Extension.swift
//  Weibo10
//
//  Created by Derek on 2020/3/29.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

extension UIButton {
    ///便利構造函數
    convenience init(imageName: String) {
        self.init()
        
        setImage(UIImage(named: imageName), for: .normal)
        
        //根據背景圖片大小調整尺寸
        sizeToFit()
    }
    
    ///便利構造函數
    convenience init(title: String, color: UIColor, bgColor: UIColor = .lightGray, fontSize: CGFloat = 16) {
        self.init()
    
        setTitle(title, for: .normal)
        setTitleColor(color, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        backgroundColor = bgColor
        layer.cornerRadius = 5
        clipsToBounds = true
        sizeToFit()
    }
    
    ///便利構造函數
    convenience init(title: String, fontSize: CGFloat, color: UIColor, image: UIImage?) {
        self.init()
    
        setTitle(title, for: .normal)
        setTitleColor(color, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        setImage(image, for: .normal)
        sizeToFit()
    }
}
