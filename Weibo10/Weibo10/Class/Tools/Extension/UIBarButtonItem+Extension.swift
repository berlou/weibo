//
//  UIBarButtonItem+Extension.swift
//  Weibo10
//
//  Created by Derek on 2020/4/9.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    
    ///便利構造函數
    convenience init(imageName: String, target: Any?, actionName: String?) {
        let button = UIButton(imageName: imageName)
        
        //判斷 actionName
        if let actionName = actionName {
            button.addTarget(target, action: Selector(actionName), for: .touchUpInside)
        }
        self.init(customView: button)
    }
}
