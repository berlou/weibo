//
//  SceneDelegate.swift
//  Weibo10
//
//  Created by Derek on 2020/3/28.
//  Copyright © 2020 Derek. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        //測試歸檔的用戶帳戶
        print(UserAccountViewModel.sharedUserAccount.account?.screen_name ?? "")
        
        setupAppearance()
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.backgroundColor = .white
        window?.rootViewController = defaultRootViewController
        window?.makeKeyAndVisible()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: WBSwitchRootViewControllerNotification),        //通知名稱，通知中心用來識別通知的
                                               object: nil, //發送通知的對象，如為 nil，監聽任何對象
                                               queue: nil)  // nil 主線程
        { [weak self] (notification) in
            let vc = notification.object != nil ? WelcomeViewController() : MainViewController()
            self?.window?.rootViewController = vc
        }
    }
    
    deinit {
        //註銷通知
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: WBSwitchRootViewControllerNotification), object: nil)
    }
    
    ///設置全局外觀
    private func setupAppearance() {
        //修改全局外觀，要在控件創建之前設置，一經設置全局有效
        UINavigationBar.appearance().tintColor = WBAppearanceTintColor
        UITabBar.appearance().tintColor = WBAppearanceTintColor
    }

    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}

//MARK: - 介面切換代碼
extension SceneDelegate {
    
    ///啟動的根視圖控制器
    private var defaultRootViewController: UIViewController {
        //1. 判斷是否登錄
        if UserAccountViewModel.sharedUserAccount.userLogon {
            return isNewVersion ? NewFeatureCollectionViewController() : WelcomeViewController()
        }
        //2. 沒有登錄返回主控制器
        return MainViewController()
    }
    ///判斷是否為新版本
    private var isNewVersion: Bool {
        //1. 當前的版本
        
        let currentVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let version = Double(currentVersion)!
        print("當前版本：", version)
        //2. 之前的版本，把當前版本保存在用戶偏好 - 如果 key 不存在，返回 0
        let sandboxVersionKey = "sandboxVersionKey"
        let sandboxVersion = UserDefaults.standard.double(forKey: sandboxVersionKey)
        print("之前版本：", sandboxVersion)
        //3. 保存當前版本
        UserDefaults.standard.set(version, forKey: sandboxVersionKey)
        
        return version > sandboxVersion
    }
}
